# -*- coding: utf-8 -*-

# %% [markdown]
# Patrick Connolly (patrickC@pembina.org, pconnolly@uvic.ca)
# June 18, 2024

# asset_matching.py

#%% [markdown]

# OK Now we have a datastructure that has all of the assets in it, indexed by the
# asset ID. Next we need to make a dictionary mapping the assets to what type they are
# and if they are a VRE source or not. This I need date for that I don't know where to find

#%% 
import numpy as np
import pandas as pd
import datetime

import time
import sys

import matplotlib.pyplot as plt

#%% 
# a switch for enabling plot outputs
plots = 0
#%%

# Load the data made by either get_historical_gen.py
# or get_historical_gen_csv.py

import pickle as pkl

assets = pkl.load(open('asset_history.pkl', 'rb'))

#%%

# This list seemingly has suppliers on it and sorted by type
names_pd = pd.read_excel('AESO_data\CSD-assets.xlsx')

# TODO I've labelled it VRE but included Hydro, should I include it?
VRE_list = ['SOLAR', 'WIND']#, 'HYDRO']

VRE_asset_IDs = []
gen_asset_IDs = []

missing_IDs = []

# go through the assets and assign their type
for row in names_pd.values:
    #unpack the row
    AID_i, name_i, type_i, sub_type_i, cap_i = row
    
    #assign to the correct asset
    try:
        assets[AID_i].type = type_i
        assets[AID_i].sub_type = sub_type_i
        assets[AID_i].name = name_i
        assets[AID_i].capacity = cap_i
        
        gen_asset_IDs.append(AID_i)
    
        # at assignment, add a reference to an asset to a lookup table if it is a VRE
        if(type_i in VRE_list):
            VRE_asset_IDs.append(AID_i)
    except KeyError:
        missing_IDs.append(AID_i)

print('there were', len(missing_IDs), 'missing IDs')
print('\n', missing_IDs)

#%% [markdown]
# Now we have a list of indexes to the VRE assets. Let's see how much renewable
# capacity is installed according to this.

#%%

VRE_cap_sum = 0
cap_sum = 0

#sum up the total generation capacity
for ID in gen_asset_IDs:
    cap_sum += assets[ID].capacity

#sum up VRE capacity
for ID in VRE_asset_IDs:
    VRE_cap_sum += assets[ID].capacity

#%%
print(cap_sum)
print(VRE_cap_sum)

print(VRE_cap_sum/cap_sum*100)

#%% [markdown]
# OK, now let's do this at a given timestamp

#%%

#TODO the only way to really index in to the time is just a number at this point
#     would be best to use an indicative date and time to both store and access

total_gen = []
VRE_gen = []

#bad way to get the time length, just take any assets time
t = assets['AKE1'].t

for i in range(len(t)):
    
    total_gen.append(0)
    VRE_gen.append(0)
    
    for ID in gen_asset_IDs:
        total_gen[i] += assets[ID].pwr_t[i]

    #sum up VRE capacity
    for ID in VRE_asset_IDs:
        #this checks for nans
        if(assets[ID].pwr_t[i] != assets[ID].pwr_t[i] ):
            print(ID)
        
        VRE_gen[i] += assets[ID].pwr_t[i]
    
total_gen = np.array(total_gen)
VRE_gen = np.array(VRE_gen)
percent_VRE = VRE_gen/total_gen*100


#%% 

# use the getTurnOnDate method to get assets to know when they turned on

tods = []
IDs_by_tod = []
for ID in VRE_asset_IDs:
    
    cur_tod = assets[ID].getTurnOnDate()
    #cur_tod = datetime.datetime.strptime(cur_tod, '%m/%d/%Y %H:%M')
    tods.append(cur_tod)
    IDs_by_tod.append(ID)

#now sort them in ascending order, this seems like a bad way to do this
sorted_inds = np.argsort(np.array(tods))

sorted_tods = np.zeros(len(tods), dtype='object')
IDs_by_sorted_tods = np.zeros(len(tods), dtype='object')

caps = np.zeros(len(tods), dtype=float)
cum_caps = np.zeros(len(tods), dtype=float)
solar_cap = np.zeros(len(tods), dtype=float)
wind_cap = np.zeros(len(tods), dtype=float)

for i in range(len(tods)):
    sorted_tods[i] = tods[sorted_inds[i]]
    IDs_by_sorted_tods[i] = IDs_by_tod[sorted_inds[i]]
    caps[i] = assets[IDs_by_sorted_tods[i]].capacity
    if(i != 0):
        cum_caps[i] = cum_caps[i-1] + caps[i]
        #sort specifically the solar
        if(assets[IDs_by_sorted_tods[i]].type == 'SOLAR'):
            solar_cap[i] = solar_cap[i-1] + caps[i]
        else:
            solar_cap[i] = solar_cap[i-1]
    else:
        cum_caps[i] = caps[i]
        #sort specifically the solar
        if(assets[IDs_by_sorted_tods[i]].type == 'SOLAR'):
            solar_cap[i] = caps[i]

#since we are only looking at wind and solar then
wind_cap = cum_caps - solar_cap

#%%

# plots of wind and solar capacity growth

if(plots):
    #plt.plot(sorted_tods, caps, marker = '.')
    # plt.figure()
    # plt.plot(sorted_tods, cum_caps, marker='.')
    # plt.fill_between(sorted_tods, cum_caps, alpha = 0.4)
    # plt.vlines(sorted_tods, np.zeros(len(tods)), cum_caps, alpha =0.5)
    # plt.xlabel('Date')
    # plt.ylabel('Installed capacity of wind and solar (MW)')
    
    widths = np.full(len(sorted_tods), datetime.timedelta(50))
    for i in range(0,len(widths)-1):
        widths[i] = sorted_tods[i+1] - sorted_tods[i]
        
    plt.figure()
    plt.bar(sorted_tods, wind_cap, width = widths, align='edge', color = 'skyblue', alpha = 0.8, label = 'wind')
    plt.bar(sorted_tods, solar_cap, bottom = wind_cap, width = widths, align='edge', color = 'goldenrod', alpha = 0.8, label = 'solar')
    
    plt.vlines(datetime.datetime(2015,1,1), 0, 5e3, alpha =0.5, color = 'r')
    plt.text(datetime.datetime(2015,2,1), 4.8e3, 'REP begins')
    
    plt.vlines(datetime.datetime(2019,1,1), 0, 5e3, alpha =0.5, color = 'r')
    plt.text(datetime.datetime(2019,2,1), 4e3, 'REP projects \nbegin coming online')

    plt.vlines(datetime.datetime(2023,8,1), 0, 5e3, alpha =0.5, color = 'r')
    plt.text(datetime.datetime(2023,9,1), 4.8e3, 'Moratorium begins')

    #plt.hist(sorted_tods, cum_caps)
    #plt.fill_between(sorted_tods, solar_cap, alpha = 0.4)
    #plt.vlines(sorted_tods, np.zeros(len(tods)), cum_caps, alpha =0.5)
    plt.xlabel('Year')
    plt.ylabel('Installed capacity of wind and solar (MW)')
    plt.grid()
    plt.legend(loc='best')
    plt.xlim(sorted_tods[0], sorted_tods[-1] + widths[-1])
    plt.tight_layout()

#%%
indmax_VRE = np.argmax(percent_VRE)
print('Max percentage of supply delivered by VRE is', percent_VRE[indmax_VRE],
      'which was achieved at', t[indmax_VRE], '. The total renewable power for taht hour was ',
      VRE_gen[indmax_VRE],'MW')

#%%

#separating this out to make it easier

EXP_BC = np.array(assets['EXPORT_BC'].pwr_t)
EXP_MT = np.array(assets['EXPORT_MT'].pwr_t)
EXP_SK = np.array(assets['EXPORT_SK'].pwr_t)


IMP_BC = np.array(assets['IMPORT_BC'].pwr_t)
IMP_MT = np.array(assets['IMPORT_MT'].pwr_t)
IMP_SK = np.array(assets['IMPORT_SK'].pwr_t)

#%%

#sample time-series plot of generation, VRE generation, and import/export
if(plots):
    start = 90000
    end = start + 2000
    
    plt.figure()
    plt.plot(t[start:end], total_gen[start:end], label = 'total')
    plt.plot(t[start:end], VRE_gen[start:end], label = 'VRE')
    plt.plot(t[start:end], EXP_BC[start:end]- IMP_BC[start:end], label = 'Net to BC')
    plt.plot(t[start:end], EXP_MT[start:end]- IMP_MT[start:end], label = 'Net to MT')
    plt.plot(t[start:end], EXP_SK[start:end]- IMP_SK[start:end], label = 'Net to SK')
    
    plt.ylabel('Generation (MW)')
    plt.xlabel('Hour from 00:00 Jan 1, 2010')
    plt.legend()
    n_xticks = 3
    #increment = lim//n_xticks
    
    ax = plt.gca()
    #ax.set_xticks(ax.get_xticks()[::increment])
    plt.tight_layout()


#%%

# plot of the hourly VRE share of generation
# plots every 1000th point cause theres too much data
if(plots):
    plt.figure()
    plt.scatter(t[::1000], VRE_gen[::1000]/total_gen[::1000]*100)
    plt.ylabel('% supply from VRE')
    plt.xlabel('date')
    
    lim = len(t)
    n_xticks = 1000
    increment = lim//n_xticks
    
    ax = plt.gca()
    ax.set_xticks(ax.get_xticks()[::increment])
    plt.tight_layout()


#%% 

record_VRE_gen = np.zeros(len(VRE_gen))
record_perc_VRE_gen = np.zeros(len(VRE_gen))

for i in range(1,len(VRE_gen)):
    
    #logic for checking if the record was broken
    if(VRE_gen[i] > record_VRE_gen[i-1]):
        record_VRE_gen[i] = VRE_gen[i]
    else:
        record_VRE_gen[i] = record_VRE_gen[i-1]
      
    #same but for percent VRE
    if(percent_VRE[i] > record_perc_VRE_gen[i-1]):
        record_perc_VRE_gen[i] = percent_VRE[i]
    else:
        record_perc_VRE_gen[i] = record_perc_VRE_gen[i-1]
        
#%%

#record plots

if(plots):
    plot_every = 1000
    plt.plot(t[::plot_every], record_VRE_gen[::plot_every], marker = '.')
    plt.grid(1)
    plt.ylabel('Record VRE Penetration (MW)')
    plt.xlabel('Year')
    plt.tight_layout()
    
    plt.figure()
    plt.plot(t[::plot_every], record_perc_VRE_gen[::plot_every], marker = '.')
    plt.grid(1)
    plt.ylabel('Record %VRE Penetration (% of supply)')
    plt.xlabel('Year')
    plt.tight_layout()
#


#%%

if(plots):
    
    fig, ax1 = plt.subplots()

    ax1.set_xlabel('Year')
    ax1.set_ylabel('Record VRE Penetration (MW)', color='blue')
    ax1.plot(t[::plot_every], record_VRE_gen[::plot_every], marker = '.', color='blue')
    ax1.grid(1)
    
    ax2 = ax1.twinx()  # instantiate a second Axes that shares the same x-axis
    
    ax2.set_ylabel('Record %VRE Penetration (% of supply)', color='red')  # we already handled the x-label with ax1
    ax2.plot(t[::plot_every], record_perc_VRE_gen[::plot_every], marker = '.', color = 'red')
    
    fig.tight_layout()  # otherwise the right y-label is slightly clipped

    plt.show()
