# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 09:21:50 2024

@author: patnc

AESO_CSD_Scrape.py

- A translation of an R script that Will Noel wrote to scrape AESO generation
  data. Keeps the same format and most naming conventions.
"""

import pandas as pd
import requests
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from dotenv import dotenv_values

# Load environment variables
denv = dotenv_values("aeso.env")

# Define API key
AESO_API_KEY = denv["X-API-Key"]

#%%
# Call API for all current asset information
# Inputs: none
def asset_list_call():
    base_url = "https://api.aeso.ca/report/"
    
    # Call current supply and demand API
    query_url = f"{base_url}v1/csd/generation/assets/current"
    response = requests.get(query_url, headers={'X-API-Key': AESO_API_KEY})

    asset_data = response.json()["return"]["asset_list"]
    df = pd.DataFrame(asset_data)
    df['fuel_type'] = df.apply(lambda x: x['sub_fuel_type'] if x['fuel_type'] == "GAS" else x['fuel_type'], axis=1)
    df = df[['asset', 'fuel_type', 'maximum_capability']]
    
    # Call asset list API
    query_url = f"{base_url}v1/assetlist?operating_status=ACTIVE&asset_type=SOURCE"
    response = requests.get(query_url, headers={'X-API-Key': AESO_API_KEY})
    asset_list_data = response.json()["return"]
    
    df = df.merge(pd.DataFrame(asset_list_data), left_on='asset', right_on='asset_ID', how='left')
    df = df[['asset_name', 'pool_participant_name', 'asset', 'fuel_type', 'maximum_capability']]
    
    return df

#%%
# Scrape Dispatcho for generation data
# Inputs: start time, end time, data frequency (1-minute, 10-minute, hourly), asset_ID's
# Example: generation_scrape("2024-01-04 16:32:00 MDT",
#                            "2024-01-05 4:17:00 MDT",
#                            "10-minute",
#                            "TVS1")

def generation_scrape(t1, t2, data_freq, selected_assets):
    max_time = {
        "1-minute": timedelta(hours=17),
        "10-minute": timedelta(days=8),
        "1-hour": timedelta(days=51)
    }[data_freq]
    
    times = [t1 + i * max_time for i in range(int((t2 - t1) / max_time) + 1)]
    times.append(t2)  # Ensure the last timestamp is included
    print(times)
    dispatch = pd.DataFrame(columns=['time_stamp', 'asset_ID', 'generation'])
    
    for asset_id in selected_assets:
        for start, end in zip(times[:-1], times[1:]):
            query_url = f"https://www.dispatcho.app/query/{asset_id}?start={int(start.timestamp())}&finish={int(end.timestamp())}"
            response = requests.get(query_url)
            data = response.json()["points"]
            
            df = pd.DataFrame(data, columns=['V1', 'V2'])
            df['asset_ID'] = asset_id
            df['time_stamp'] = pd.to_datetime(df['V1'].cumsum(), unit='s', utc=True).dt.tz_convert('MST')
            df['generation'] = df['V2'].cumsum() / 10
            
            dispatch = pd.concat([dispatch, df[['time_stamp', 'asset_ID', 'generation']]], ignore_index=True)
    
    return dispatch

#%% Scrape and sort

t1 = datetime(2024, 7, 8, 0, 0)
t2 = datetime(2024, 7, 13, 0, 0)
data_freq = "1-hour"

# Call AESO API for generator info
asset_list = asset_list_call()

# Generate list of renewable assets
renewables = asset_list[asset_list['fuel_type'].isin(['WIND', 'SOLAR'])]['asset'].tolist()

# Query generation data
df = generation_scrape(t1, t2, data_freq, renewables)

# Summarize by fuel type
merged_df = df.merge(asset_list[['asset', 'fuel_type']], left_on='asset_ID', right_on='asset')
summary_df = merged_df.groupby(['time_stamp', 'fuel_type'], as_index=False).agg({'generation': 'sum'})

#%% Visualize

# line plot
# Plot data as a stacked area graph
plt.figure(figsize=(10, 6))
for fuel_type in ['WIND', 'SOLAR']:
    plt.plot(summary_df.loc[summary_df['fuel_type'] == fuel_type, 'time_stamp'],
             summary_df.loc[summary_df['fuel_type'] == fuel_type, 'generation'],
             label=fuel_type)

plt.title("Alberta Renewables")
plt.ylabel("MW")
plt.legend(title="Fuel Type")
plt.xticks(rotation=60)
plt.tight_layout()
plt.show()

#%% #stacked area

plt.figure(figsize=(10, 6))
plt.grid()
plt.stackplot(summary_df.loc[summary_df['fuel_type'] == 'SOLAR', 'time_stamp'],
              summary_df.loc[summary_df['fuel_type'] == 'WIND', 'generation'],
              summary_df.loc[summary_df['fuel_type'] == 'SOLAR', 'generation'],
              labels = ['Wind','Solar'])

plt.title("Alberta Renewables")
plt.ylabel("MW")
plt.legend(title="Fuel Type")
plt.xticks(rotation=0)
plt.tight_layout()
plt.show()
