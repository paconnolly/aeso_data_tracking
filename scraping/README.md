# Scraping

AESO only publishes the most recent data on their CSD website and does not make it convenient to access. This section is code (based on Will Noel's) to get the data from a website that is scraping their data. The end goal of this is to have it run automatically and keep a record of generation, as well as use the logic from other functions to identify when new records have been set.

To Do:
1. [x] Functionality to scrape the data from [Dispatcho](https://www.dispatcho.app/assets)
This is done by **AESO_CSD_scrape.py**
2. [ ] Functionality to save the data sequentially

3. [ ] Functionality to run the script periodically to keep data up to date

4. [ ] Funtionality to identify records and notify people

