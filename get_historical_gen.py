# -*- coding: utf-8 -*-
# %% [markdown]
# Patrick Connolly (patrickC@pembina.org, pconnolly@uvic.ca)
# June 17, 2024

# Process to follow
# - open the webpage
# - read in the webpage HTML to get the table
# - - Note, each day is in a different table
# - parse what was read in in to a table
# - return the table

# I assume an "hour ending in" convention in the AESO data, since the website 
# lists hours from  1-24 when you ask for a specific date. However, the
# convention for datetime objects in python is the 24 hour clock 
# i.e [0:00, 23:59], which lends itself better to an "hour beginning in"
# convention, so outputs are in that format

# %% [markdown]

# #get_historical_gen

# This file gets the historical hourly generation dat from and AESO web table.
# Link (http://ets.aeso.ca/ets_web/ip/Market/Reports/PublicSummaryAllReportServlet?beginDate=01012024&endDate=01112024&contentType=html)

# Will probably be formatted into a function rather than just a script, so the
# inputs and outputs would be

# inputs:
#     - dates_in, either as the url or a list of [start_date, end_date]
    
# returns:
#     - pandas dataframe with labelled generation data
# '''

#%%

import numpy as np
import pandas as pd
import datetime
from pytz import timezone


#%%
#function input
dates_in = 'http://ets.aeso.ca/ets_web/ip/Market/Reports/PublicSummaryAllReportServlet?beginDate=01012024&endDate=01032024&contentType=html'
#%%

# read the html from the website
URL =''
if(type(dates_in) == list):
    start_date = dates_in[0]
    end_date = dates_in[1]
    
    #make the URL from the dates provided
else:
    #parse the url provided for the dates
    
    URL = dates_in

#for some reason this takes a really long time to run
table_pd = pd.read_html(URL)

#%% 
# Post-process the table 

#There are 2 + 2 x no. of days tables
# - two headers are title and date accessed
# - first repeating is corresponding date
# - second repeating is the full data table

tables = table_pd[2:]

n_days = len(tables)//2

dates = np.zeros(n_days, dtype=datetime.datetime)
headers = np.zeros(len(tables[-1].columns), dtype = object)
data_tables = np.zeros(n_days, dtype = pd.DataFrame)

#assemble the headers
# NOTE: right now i'm ignoring the grey-background line under the hour
#       because I don't know what it is

headers[0:3] = ['Pool Particpant ID', 'Asset Type', 'Asset ID'] #hardcodign this for now because it was inconvenient to do it a better way
headers[3:27] = tables[-1].values[0][0:24]

#arrange tables better
tz = timezone('US/Mountain')

for i in range(n_days):
    #save the date of the table separtely
    dates[i] = datetime.datetime.strptime(tables[2*i].values[0,0], '%B %d, %Y.')
    dates[i] = dates[i].replace(tzinfo=tz)
    #data_tables is just going to be the clean version of tables
    data_tables[i] = pd.DataFrame(tables[2*i + 1].drop([0,1]).values,
                                  columns = headers)

#%%
import asset
import matplotlib.pyplot as plt

assets = {}

# TODO: make this work for datetime objects so that time differences are automatically accounted for.

hrs = np.arange(0, 24)
#tds = np.zeros(24, dtype=object)
#for i in range(len(tds)):
#    tds[i] = datetime.timedelta(hours = int(hrs[i]))
    
#dates = np.full(n_hrs, dates[0])
#dates = dates + tds

for i in range(n_days):
    dti = data_tables[i]
    
    date_i = dates[i]
    datetimes_i = i*24 + hrs
    
    for j in range(len(data_tables[i].values)):
        #get the AID
        AID_ij = dti.values[j,2]
        
        if(not (AID_ij in list(assets.keys()))):
            #add it to the list of assets
            PPID_j = dti.values[j,0]
            AType_j = dti.values[j,1]
            asset_j = asset.asset(PPID_j, AType_j, AID_ij)
            
            assets[AID_ij] = asset_j
            
        #either way, append the time-series to that assets track
        assets[AID_ij].appendPowerData(list(datetimes_i), list(dti.values[j][3:]))
            
            
#test
print(assets['CES1'].t)
plt.plot(assets['CES1'].t, assets['CES1'].pwr_t)

#%% [markdown]

# OK Now we have a datastructure that has all of the assets in it, indexed by the
# asset ID. Next we need to make a dictionary mapping the assets to what type they are
# and if they are a VRE source or not. This I need date for that I don't know where to find

#%%



#%% 

# TODO this might not be the best way to do this, since generators list might
# change over time. I think I'll keep this on the todo list for now

# Great, now there are tables for each day of hourly generation with the
# relevant classifyign information in the rows with it. Next put it all into
# one big dataframe with the column header indicating the full date and time    
n_hrs = 24*n_days
hrs = np.arange(0, n_hrs)
tds = np.zeros(n_hrs, dtype=object)
for i in range(len(tds)):
    tds[i] = datetime.timedelta(hours = int(hrs[i]))
    
dates = np.full(n_hrs, dates[0])
dates = dates + tds

headers = ['Pool Particpant ID', 'Asset Type', 'Asset ID']

for i in range(n_hrs):
    headers.append(dates[i])

