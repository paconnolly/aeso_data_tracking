# -*- coding: utf-8 -*-
# %% [markdown]
# Patrick Connolly (patrickC@pembina.org, pconnolly@uvic.ca)
# June 17, 2024

# # Asset objects
# Thought it would be useful to create an asset object that will track all of
# it's own values i.e Asset type, ID, time-series, etc.

# # Properties
# PPID = Pool participant ID
# AType = Asset Type
# AID = Asset ID
# t = time associated with power time series
# pwr_t = time series of power produced etc.
# is_vre = flag for if the asset is a variable renewable energy generator

#%% 
# Asset 
class asset:

    def __init__(self, PPID, AType, AID, is_vre = 0):
        self.PPID = PPID
        self.AType = AType
        self.AID = AID
        self.name = ''
        
        self.type = ''
        self.sub_type = ''
        self.capacity = 0.
        
        self.pwr_t = []
        self.t = []
        
        self.is_vre = is_vre
        
        self.turn_on_date = 0
        
    def appendPowerData(self, t_n, pwr_t_n):
        #assume they're lists of equal size

        for i in range(len(t_n)):
            
            #correct for nans
            if(float(pwr_t_n[i]) != float(pwr_t_n[i])):
                self.pwr_t.append(0.0)
            else:
                self.pwr_t.append(float(pwr_t_n[i]))

            self.t.append(t_n[i])
            
    def getTurnOnDate(self):
        
        if(self.turn_on_date != 0):
            return self.turn_on_date
        else:
            #determine when the asset turned on, assuming that it already has
            #its meter history
            pwr_on = 0
            ind = 0
            while(not pwr_on):
                if(self.pwr_t[ind] > 0.):
                    pwr_on = 1
                    self.turn_on_date = self.t[ind]
                else:
                    ind += 1
                    
            return self.turn_on_date

