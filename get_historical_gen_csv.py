# -*- coding: utf-8 -*-
# %% [markdown]
# Patrick Connolly (patrickC@pembina.org, pconnolly@uvic.ca)
# June 18, 2024

#%% [markdown]

# so accessing the data from the web takes a long time. It takes ~1 min for 
# 2 days of data and ~ 4 for 3 days. 

# here we alternatively, read data from the CSV from here: 
# https://aeso.ca/market/market-and-system-reporting/data-requests/hourly-metered-volumes-and-pool-price-and-ail-data-2010-to-2023/

# This is much faster and should also work fine. The only problem is that this
# table is transposed relative to the other table...

#%%

import numpy as np
import pandas as pd
import datetime

import time
import sys

import pickle as pkl

#%% 
path = 'AESO_data\Hourly_Metered_Volumes_and_Pool_Price_and_AIL.csv'

start = time.time()

table_pd = pd.read_csv(path)
end = time.time()

print('It took: ', end-start, 's to load the csv')
table_pd = table_pd.T

#%% [markdown]

# Process that follows will be similar but different from the
# get_historical_gen.py process. 

#%% 
# Post-process the table 

dates_str = table_pd.values[1] #date and time in local time, hour beginning in format
dates = np.full(len(dates_str), datetime.datetime(1,1,1))
for i in range(len(dates_str)):
    dates[i] = datetime.datetime.strptime(dates_str[i], '%m/%d/%Y %H:%M')

#%%
import asset
import matplotlib.pyplot as plt

assets = {}
AIDs = list(table_pd.index)
# TODO: make this work for datetime objects so that time differences are automatically accounted for.

hrs = np.arange(0, 24)
table_arr = table_pd.values
for i in range(2,len(table_pd)):
    
    
    #get the AID
    AID_i = AIDs[i]
        
    if(not (AID_i in list(assets.keys()))):
        #add it to the list of assets
        asset_i = asset.asset('','', AID_i)
        
        assets[AID_i] = asset_i
        
    #either way, append the time-series to that assets track
    assets[AID_i].appendPowerData(dates, table_arr[i][:])

#%%

# Now pickle the assets object
out_path = 'asset_history.pkl'
pkl.dump(assets, open(out_path, 'wb'))

#%% test
plots = 0

if(plots):
    lim = 1000
    n_xticks = 3
    
    increment = lim//n_xticks
    print(assets['AKE1'].t)
    plt.plot(assets['GN2'].t[:lim], assets['GN2'].pwr_t[:lim])
    ax = plt.gca()
    ax.set_xticks(ax.get_xticks()[::increment])
